﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tekever.more.connector.python.oss.optimizer.Utils
{
    class Constants
    {
        public struct Attr
        {
            public const string outputfilename = "outputfilename";
            public const string user = "user";
            public const string name = "name";
            public const string code = "code";
            public const string address = "address";
            public const string solution = "solution";
            public const string orgunit = "orgunit";
            public const string number = "number";
            public const string shiftStart = "shiftStart";
            public const string shiftFinish = "shiftFinish";

            public const string possibleStartDate = "possibleStartDate";
            public const string possibleFinishDate = "possibleFinishDate";
            public const string latitude = "latitude";
            public const string longitude = "longitude";
            public const string otherParams = "otherParams";
            public const string teams = "teams";
            public const string tasks = "tasks";
            public const string matrix = "matrix";
            public const string scheduledWithClient = "scheduledWithClient";
            public const string point = "point";
            public const string timeBlock = "timeBlock";

            public const string traffic = "traffic";
            public const string useCache = "useCache";
            public const string maps = "maps";
            public const string teamEfficiency = "teamEfficiency";
            public const string skills = "skills";
            public const string coef = "coef";
            public const string reqSkill = "reqSkill";

            public const string error = "error";
            public const string logMaster = "logMaster";
            public const string orderLogs = "orderLogs";
            public const string cronSetupCode = "cronSetupCode";
            public const string targetDate = "targetDate";
            public const string targetWorkCenter = "targetWorkCenter";
            public const string unscheduled = "unscheduled";
            public const string intra_lat = "intra_lat";
            public const string intra_lon = "intra_lon";

            //OptimizerSetup
            public const string abBuffer = "abBuffer";
            public const string slaTolerance = "slaTolerance";
            public const string lunchTime = "lunchTime";
            public const string costPerMeter = "costPerMeter";
            public const string costPerMinuteTask = "costPerMinuteTask";
            public const string costPerMinuteFree = "costPerMinuteFree";
            public const string costPerMinuteTrip = "costPerMinuteTrip";
            
            //Vehicles
            public const string howManyTeams = "howManyTeams";
            public const string vehicles = "vehicles";
            public const string home_lat = "home_lat";
            public const string home_lon = "home_lon";

            //ReOptimize
            public const string ReOptimize = "ReOptimize";
            public const string teamCodesNotIn = "teamCodesNotIn";
        }

        public struct Process
        {
            public const string myFork = "MyFork";
        }
    }
}
