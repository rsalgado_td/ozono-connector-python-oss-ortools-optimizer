# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 15:37:14 2018

@author: António Santos
"""

 ## Import
import sys
import json
import csv
from datetime import datetime
from datetime import timedelta
import numpy as np
from six.moves import xrange
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2

class DataProblem(object):
    def __init__(self, data):
        
        # Get info from json
        tasks = get_tasks(data)
        teams = get_teams(data)
        oParams = get_otherParams(data)
        targetDate = data['targetDate']
        self._targetdate = targetDate
        
        # Setup costs
        self._distcost = oParams[6]
        if self._distcost == 0:
            self._distcost = 100
            
        self._cPMTsk = oParams[4]
        if self._cPMTsk is None:
            self._cPMTsk = 0.01 
        
        self._cPM = oParams[2]
        if self._cPM is None:
            self._cPM = 0.01
        
        self._cPMTrp = oParams[5]
        if self._cPMTrp is None:
            self._cPMTrp = 0.05
        
        self._cPMFr = oParams[3]
        if self._cPMFr is None:
            self._cPMFr = 0.03
            
        self._timecost = oParams[10]
        if self._timecost == 0:
            self._timecost = 100
            
        # Setup locations (inclunding lunch)
        location = (teams[3])
        self._lFL = teams[2]
        if sum(self._lFL) == 0:
            isLunchFixed = False # tem de ser lido do json
        else:
            isLunchFixed = True
            
        self._ilf = isLunchFixed
        if isLunchFixed:
            t_lunch =[]
            lunch  = list(range(len(teams[0])))
            for a in lunch:
                if self._lFL[a]:
                    t_lunch.append(a + len(teams[0]))
                    location.append(teams[3][a])
            self._lunch_loc = t_lunch
        else:
            self._lunch_loc = []
        location += tasks[0]
        self._locations = location
        
        # Setup work order id
        self._wo_id = (teams[5])
        if isLunchFixed:
            self._wo_id += (teams[5])
        self._wo_id += tasks[5]
        
        # Setup time windows, demands, scheduled with client
        num_teams = len(teams[0])
        self._eff_toggle = oParams[15] # use efficiency?
        t_windows = []
        dem = []
        swc = []
        wot = []
        nme = []
        opS = []
        wrc = []
        wos = []
        wst = []
        RqS = []
        pSd = []
        pFd = []
        act = []
        tcd = []
        
        # Setup activity types
        #self._acti = (teams[4])
        #self._acti += tasks[7]        
        
        # Homes
        for a in range(num_teams):
            stTime = teams[7][a] - 8*60 # fixar a origem às 8 da manhã
            interval = (stTime, stTime)
            t_windows.append(interval)
            dem.append(0)
            swc.append(0)
            wot.append("home")
            nme.append(tasks[12][a])
            opS.append(tasks[13][a])
            wrc.append(tasks[14][a])
            wos.append(tasks[15][a])
            wst.append(tasks[16][a])
            RqS.append(tasks[11][a])
            pSd.append(tasks[17][a])
            pFd.append(tasks[18][a])
            act.append(teams[4][a])
            tcd.append(tasks[5][a])
        # Home lunch    
        if isLunchFixed:
            for a in range(num_teams):
                if self._lFL[a]:
                    stTime = teams[9][a] - 8*60 # fixar a origem às 8 da manhã
                    edTime = teams[1][a] - 8*60 # fixar a origem às 8 da manhã
                    interval = (stTime, stTime)
                    t_windows.append(interval)
                    dem.append(1)
                    swc.append(1)
                    wot.append("lunch")
                    nme.append(tasks[12][a])
                    opS.append(tasks[13][a])
                    wrc.append(tasks[14][a])
                    wos.append(tasks[15][a])
                    wst.append(tasks[16][a])
                    RqS.append(tasks[11][a])
                    pSd.append(tasks[17][a])
                    pFd.append(tasks[18][a])
                    act.append(teams[4][a])
                    tcd.append(tasks[5][a])
        # Tasks
        num_tasks = len(tasks[0])
        for b in range(num_tasks):
            stTime = tasks[2][b] - 8*60 # fixar a origem às 8 da manhã
            edTime = tasks[3][b] - 8*60 # fixar a origem às 8 da manhã
            interval = (stTime, edTime)
            t_windows.append(interval)
            dem.append(1)
            swc.append(tasks[4][b])
            wot.append(tasks[10][b])
            nme.append(tasks[12][b])
            opS.append(tasks[13][b])
            wrc.append(tasks[14][b])
            wos.append(tasks[15][b])
            wst.append(tasks[16][b])
            RqS.append(tasks[11][b])
            pSd.append(tasks[17][b])
            pFd.append(tasks[18][b])
            act.append(tasks[7][b])
            tcd.append(tasks[5][b])
          
        self._acti = act    
        self._RqSkill = RqS
        self._task_name = nme
        self._task_optSol = opS
        self._workCenter = wrc
        self._workOrderStatus = wos
        self._workOrderSubtype = wst
        self._time_windows = t_windows
        self._demands = dem
        self._client = swc
        self._possibleStartDate = pSd
        self._possibleFinishDate = pFd
        # Setup work order type
        self._workordertype = wot 
        self._task_code = tcd

        # Set depot indexes and free depot toggle
        self._depot = list(range(num_teams))
        self._freeDepot= bool(oParams[8])
        
        # Setup vehicles
        self._num_vehicles = len(self._depot)
        self._vehicle = Vehicle(teams, self._eff_toggle)
        
        # Setup skill requirement
        skills= []
        for c in range(num_teams):
            skills.append('')
        if isLunchFixed:
            for c in range(num_teams):
                skills.append('')
        skills += tasks[6]
        self._skill= skills
        
        # Setup time demand per unit
        tpdu= []
        for c in range(num_teams):
            tpdu.append(0)
        if isLunchFixed:
            for c in range(num_teams):
                if self._lFL[c]:
                    stTime = teams[9][c] - 8*60 # fixar a origem às 8 da manhã
                    edTime = teams[1][c] - 8*60 # fixar a origem às 8 da manhã
                    tpdu.append(edTime - stTime)
        tpdu += tasks[8]
        self._time_per_demand_unit = tpdu  
        
        # Setup priority
        prio = []
        for c in range(num_teams):
            prio.append(0)
        if isLunchFixed:
            for c in range(num_teams):
                if self._lFL[c]:
                    prio.append(100)
        prio += tasks[9]
        self._priority = prio
        
        # Extend timetable
        self._extTTable = bool(oParams[14])
        
        # Lunch
        self._lunch = bool(oParams[13])
        
        # Google Maps
        self._gmaps = bool(oParams[9])
        self._gcache = bool(oParams[12])
        self._traffic = bool(oParams[11])
        
        # Get distance matrix
        if self._gmaps:
            try:
                self._dmtx = data['matrix']
            except:
                filename_out = "error.json"
                output = {'message' : 'distance matrix missing'}
                with open(filename_out, 'w') as outfile:
                    json.dump(output, outfile, sort_keys = True, indent = 4)
        else:
            self._dmtx = False
         
        # Other optimizer parameters        
        self._optCode = oParams[0]
        self._cost = oParams[1]
        self._defSetup = oParams[16]
        self._descrip = oParams[17]
        self._extTeams = oParams[7]
        self._optid = oParams[18]
        self._optName =  oParams[19]
        self._networkU = oParams[20]
        self._orgunit = oParams[21]
        self._sla = oParams[22]
        
    @property
    def num_vehicles(self):
        """Gets number of vehicles"""
        return self._num_vehicles

    @property
    def locations(self):
        """Gets locations"""
        return self._locations

    @property
    def num_locations(self):
        """Gets number of locations"""
        return len(self.locations)

    @property
    def task_code(self):
        return self._task_code

    @property
    def wo_id(self):
        return self._wo_id

    @property
    def depot(self):
        """Gets depot location index"""
        return self._depot

    @property
    def time_windows(self):
        return self._time_windows
    
    @property
    def vehicle(self):
        """Gets a vehicle"""
        return self._vehicle
    
    @property
    def demands(self):
        return self._demands
    
    @property
    def time_per_demand_unit(self):
        """Gets the time (in min) to load a demand"""
        return self._time_per_demand_unit 
    
    @property
    def skill(self):
        return self._skill
    
    @property
    def acti(self):
        return self._acti
    
    @property
    def client(self):
        return self._client
    
    @property
    def freeDepot(self):
        return self._freeDepot
 
    @property
    def priority(self):
        return self._priority
    
    @property
    def extTTable(self):
        return self._extTTable
    
    @property
    def lunch(self):
        return self._lunch    
    
    @property
    def gmaps(self):
        return self._gmaps
    
    @property
    def gcache(self):
        return self._gcache
    
    @property
    def traffic(self):
        return self._traffic 

    @property
    def dmtx(self):
        return self._dmtx 

    @property
    def workordertype (self):
        return self._workordertype     
    
    @property
    def targetdate (self):
        return self._targetdate     

    @property
    def distcost (self):
        return self._distcost 

    @property
    def cPMTsk (self):
        return self._cPMTsk
    
    @property
    def cPM (self):
        return self._cPM
    
    @property
    def cPMTrp (self):
        return self._cPMTrp

    @property
    def cPMFr (self):
        return self._cPMFr
    
    @property
    def timecost (self):
        return self._timecost

    @property
    def lunch_loc (self):
        return self._lunch_loc

    @property
    def ilf (self):
        return self._ilf
    
    @property
    def lFL (self):
        return self._lFL
    
    @property
    def eff_toggle (self):
        return self._eff_toggle
    
    @property
    def optCode (self):
        return self._optCode

    @property
    def cost (self):
        return self._cost    

    @property
    def defSetup (self):
        return self._defSetup 

    @property
    def descrip (self):
        return self._descrip 

    @property
    def extTeams (self):
        return self._extTeams

    @property
    def optid (self):
        return self._optid

    @property
    def optName (self):
        return self._optName

    @property
    def networkU (self):
        return self._networkU

    @property
    def orgunit (self):
        return self._orgunit        

    @property
    def sla (self):
        return self._sla   

    @property
    def RqSkill (self):
        return self._RqSkill

    @property
    def task_name (self):
        return self._task_name    

    @property
    def task_optSol (self):
        return self._task_optSol  
    
    @property
    def workCenter (self):
        return self._workCenter  

    @property
    def workOrderStatus (self):
        return self._workOrderStatus  

    @property
    def workOrderSubtype (self):
        return self._workOrderSubtype 
     
    @property
    def possibleStartDate(self):
        return self._possibleStartDate
        
    @property
    def possibleFinishDate(self):
        return self._possibleFinishDate 
    
def haversine(lon1, lat1, lon2, lat2):
        """
        Calculate the great circle distance between two points
        on the earth specified in decimal degrees of latitude and longitude.
        https://en.wikipedia.org/wiki/Haversine_formula

        Args:
            lon1: longitude of pt 1,
            lat1: latitude of pt 1,
            lon2: longitude of pt 2,
            lat2: latitude of pt 2

        Returns:
            the distace in km between pt1 and pt2
        """
        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = (np.sin(dlat / 2) ** 2 + np.cos(lat1) *
             np.cos(lat2) * np.sin(dlon / 2) ** 2)
        c = 2 * np.arcsin(np.sqrt(a))

        # 6367 km is the radius of the Earth
        mm = 6367000 * c
        return mm

def manhattan(lon1, lat1, lon2, lat2):
    latSize = 110 # block latitude direction length
    lonSize = 85  # block longitude direction length
    
    dlon = abs(lon2-lon1)
    dlat = abs(lat2-lat1)
    
    dist = dlat * latSize + dlon * lonSize
    return int(dist)

def distance(from_node, to_node):
    lon1 = from_node[1]
    lat1 = from_node[0]
    lon2 = to_node[1]
    lat2 = to_node[0]
    
    dist = manhattan(lon1, lat1, lon2, lat2)
    return int(dist)

class Vehicle():
    """Stores the property of a vehicle"""
    def __init__(self, teams, eff_toggle):
        """Initializes the vehicle properties"""
        # Travel speed: 30 km/h to convert in m/min
        self._speed = 0.5 #km/min
        self._capacity = 100
        self._skill = teams[8]
        self._startbreak = teams[9]
        self._endbreak= teams[1]
        self._startshift = teams[7]
        self._endshift = teams[6]
        self._name = teams[4]
        self._code = teams[0]
        
        if eff_toggle == 1:
            self._efficiency = teams[13]
        else:
            self._efficiency = [1]*len(self._name)
        
    @property
    def code(self):
        return self._code
    
    @property
    def name(self):
        return self._name
        
    @property
    def skill(self):
        return self._skill

    @property
    def startbreak(self):
        return self._startbreak
    
    @property
    def endbreak(self):
        return self._endbreak

    @property
    def startshift(self):
        return self._startshift

    @property
    def endshift(self):
        return self._endshift
    
    @property
    def efficiency(self):
        return self._efficiency
    
    @property
    def speed(self):
        """Gets the average travel speed of a vehicle"""
        return self._speed

    @property
    def capacity(self):
        return self._capacity
        
## Tasks   
def get_tasks(data):
    num_task = len(data['tasks'])

    act = [] # activity type
    dur = [] # duration
    code = [] # activity code
    loc = [] # location (lat, lon)
    pFD = [] # possible finish date
    pSD = [] # possible start date
    pri = [] # priority
    rSk = [] # required skill
    swc = [] # scheduled with client
    dem = [] # demand (para o ortools)
    wot = [] # work order type
    nme = [] # work order name
    RqS = [] # ReqSkill (unused)
    opS = [] # optimizer solution
    
    wrc = [] # work center 
    wos = [] # work order status
    wst = [] # work order subtype
    sdp = [] # scheduled start date for print
    fdp = [] # scheduled end date for print
    for a in range(num_task):
        act.append(data['tasks'][a]['activityType'])
        duration = int(data['tasks'][a]['actualDuration'])
        if duration == 0:
            duration = 20
        dur.append(duration)
        code.append(data['tasks'][a]['code'])
        lat = (float(data['tasks'][a]['latitude']))
        lon = (float(data['tasks'][a]['longitude']))
        loc.append((lat, lon))
        finDate = data['tasks'][a]['possibleFinishDate']
        fdp.append(finDate)
        tempFD = datetime.strptime(finDate, '%Y-%m-%d %H:%M:%S')
        pFD.append(tempFD.hour*60 + tempFD.minute)
        
        staDate = data['tasks'][a]['possibleStartDate']
        tempSD = datetime.strptime(staDate, '%Y-%m-%d %H:%M:%S')
        sdp.append(staDate)
        pSD.append(tempSD.hour*60 + tempSD.minute)
        
        pri.append(int(data['tasks'][a]['priority']))
        skill_vec = data['tasks'][a]['reqSkill']
        skill = ''
        for b in skill_vec:
            skill += b + '|'
        rSk.append(skill)
        swc.append(int(data['tasks'][a]['scheduledWithClient']))
        dem.append(1)
        wot.append(data['tasks'][a]["workOrderType"])
        nme.append(data['tasks'][a]['name'])
        RqS.append(data['tasks'][a]['ReqSkill'])
        opS.append(data['tasks'][a]['optimizerSolution'])
        wrc.append(data['tasks'][a]['workCenter'])
        wos.append(data['tasks'][a]['workOrderStatus'])
        wst.append(data['tasks'][a]['workOrderSubtype'])
        
    task_out = [loc, dem, pSD, pFD, swc, code, rSk, act, dur,pri, wot,RqS, nme, opS, wrc, wos, wst, sdp, fdp]
    return(task_out)
    
## Teams
def get_teams(data):
    num_teams = len(data['teams'])
    
    code = [] # code
    eBW = [] # endBreakWindow
    lFL = [] # isLunchFixedLocation
    loc = [] # location (lat, lon) (depot)
    name = [] # name
    num = [] # number
    sFn = [] # shift finish
    sSt = [] # shift start
    skl = [] # skills
    sBW = [] # startBreakWindow
    sup = [] # supplier
    tSt = [] # team status
    tTp = [] # team type
    tEf = [] # team efficiency coeficient
    for b in range(num_teams):
        code.append(str(data['teams'][b]['code']))
        eBW.append(int(data['teams'][b]['endBreakWindow']))
        lFL.append(bool(data['teams'][b]['isLunchFixedLocation']))
        lat = (float(data['teams'][b]['latitude']))
        lon = (float(data['teams'][b]['longitude']))
        loc.append((lat, lon))
        name.append(str(data['teams'][b]['name']))
        num.append(str(data['teams'][b]['number']))
        sFn.append(int(data['teams'][b]['shiftFinish']))
        sSt.append(int(data['teams'][b]['shiftStart']))
        skill_vec = data['teams'][b]['skills']
        skill = ''
        for a in skill_vec:
            skill += a + '|'
        skl.append(skill)
        sBW.append(int(data['teams'][b]['startBreakWindow']))
        sup.append(data['teams'][b]['supplier'])
        tSt.append(data['teams'][b]['teamStatus'])
        tTp.append(data['teams'][b]['teamType'])
        tEf.append(int(data['teams'][b]['coef']))
    out_teams = [code, eBW, lFL, loc, name, num, sFn, sSt, skl, sBW, sup, tSt, tTp, tEf]
    return(out_teams)
    
## Other Parameters
def get_otherParams(data):
    code = data['otherParams']['code'] # code
    cost = int(data['otherParams']['cost']) # cost
    cPM = data['otherParams']['costPerMeter']
    cPMF = data['otherParams']['costPerMinuteFree']
    cPMTsk = data['otherParams']['costPerMinuteTask']
    cPMTrp = data['otherParams']['costPerMinuteTrip']
    dist = float(data['otherParams']['distance'])
    exT = int(data['otherParams']['externalTeams'])
    fDpt = int(data['otherParams']['freeDepot'])
    maps = int(data['otherParams']['maps'])
    tmcf = float(data['otherParams']['time'])
    trf = int(data['otherParams']['traffic'])
    uCch = int(data['otherParams']['useCache'])
    wLnc = int(data['otherParams']['withLunch'])
    uExt = int(data['otherParams']['useExtension']) # estender horário de trabalho para as 8 às 22
    tEff = int(data['otherParams']['teamEfficiency'])
    dfSt = data['otherParams']['defaultSetup']
    desc = data['otherParams']['description']
    oid = data['otherParams']['id']
    name = data['otherParams']['name']
    netu = data['otherParams']['networkUnit']
    orgu = data['otherParams']['orgunit']
    slaT = data['otherParams']['slaTolerance']
    
    out_params = [code, cost, cPM, cPMF, cPMTsk, cPMTrp, dist, exT, fDpt, maps,
                  tmcf, trf, uCch, wLnc, uExt, tEff, dfSt, desc, oid, name, netu, orgu, slaT]
    return(out_params)

    
class CreateTimeEvaluator(object):
    """Creates callback to get total times between locations."""
    @staticmethod
    def service_time(data, node):
        """Gets the service time for the specified location."""
        return data.demands[node] * data.time_per_demand_unit[node]

    @staticmethod
    def travel_time(data, from_node, to_node):
        """Gets the travel times between two locations."""      
        if data.gmaps:
            if from_node == to_node:
                travel_time = 0
            else: 
                orig = data.locations[from_node]
                dest = data.locations[to_node]
                lat0 = str(orig[0]); lon0 = str(orig[1]); lat1 = str(dest[0]);
                lon1 = str(dest[1])                
                segm = lat0 + ',' + lon0 + '/' + lat1 + ',' + lon1
                if data.traffic:
                    key = 'duration_in_traffic'
                else:
                    key = 'duration'
                travel_time = int(data.dmtx[0].get(segm)[key])
                trip_distance = int(data.dmtx[0].get(segm)['distance'])
                if trip_distance < 1:
                    travel_time = 5
        else:
            if from_node == to_node:
                travel_time = 0
            else:
                travel_time = distance(
                        data.locations[from_node],
                        data.locations[to_node]) / data.vehicle.speed
                trip_distance = distance(
                        data.locations[from_node],
                        data.locations[to_node])
                if trip_distance < 1:
                    travel_time = 5
        return travel_time

    def __init__(self, data):
        """Initializes the total time matrix."""
        self._total_time = {}
        # precompute total time to have time callback in O(1)
        for from_node in xrange(data.num_locations):
            self._total_time[from_node] = {}
            for to_node in xrange(data.num_locations):
                if from_node == to_node:
                    self._total_time[from_node][to_node] = 0
                else:
                    self._total_time[from_node][to_node] = int(
                        self.service_time(data, from_node) +
                        self.travel_time(data, from_node, to_node))
                
    def time_evaluator(self, from_node, to_node):
        """Returns the total time between the two nodes"""
        return self._total_time[from_node][to_node]
    
def add_time_window_constraints(routing, data, time_evaluator, solver):
    """Add Global Span constraint"""
    time = "Time"
    if ~data.extTTable:
        horizon = 10*60 # das 8 às 18
    else:
        horizon = 14*60 # das 8 às 22
    routing.AddDimension(
        time_evaluator,
        45, # allow waiting time (meter o valor max de espera em minutos)
        horizon, # maximum time per vehicle
        False, # start cumul to zero 
        time)
    time_dimension = routing.GetDimensionOrDie(time)
    for location_idx, time_window in enumerate(data.time_windows):
        time_dimension.CumulVar(location_idx).SetRange(time_window[0], time_window[1])
     
    # Almoço e turnos por equipa
    for vn in range(data.num_vehicles):
        if not data.lunch:
            continue
        elif data.lunch and not data.lFL[vn]:
            stBreak = data.vehicle.startbreak[vn] - 8*60 # fixar a origem às 8 da manhã
            edBreak = data.vehicle.endbreak[vn] - 8*60 # fixar a origem às 8 da manhã
            durBreak = edBreak - stBreak
            t_breaks = [solver.FixedInterval(stBreak, durBreak, 'Hora de almoço')]
            time_dimension.SetBreakIntervalsOfVehicle(t_breaks, vn)
        else:
            node = data.lunch_loc[vn]
            location_idx = routing.NodeToIndex(node)
            routing.VehicleVar(location_idx).SetValues([-1,vn])
        stShft = data.vehicle.startshift[vn] - 8*60 # fixar a origem às 8 da manhã
        edShft = data.vehicle.endshift[vn] - 8*60 # fixar a origem às 8 da manhã
        time_dimension.CumulVar(routing.Start(vn)).SetMin(stShft) 
        time_dimension.CumulVar(routing.End(vn)).SetMax(edShft) 
                   
class CreateDistanceEvaluator(object): # pylint: disable=too-few-public-methods
    """Creates callback to return distance between points."""
    def __init__(self, data):
        """Initializes the distance matrix."""
        self._distances = {}
        # precompute distance between location to have distance callback in O(1)
        for from_node in xrange(data.num_locations):
            self._distances[from_node] = {}
            for to_node in xrange(data.num_locations):
                    if from_node == to_node:
                        self._distances[from_node][to_node] = 0
                    elif data.freeDepot and (from_node in data.depot or to_node in data.depot):
                        self._distances[from_node][to_node] = 0 
                    elif data.ilf and (from_node in data.lunch_loc or to_node in data.lunch_loc):
                        self._distances[from_node][to_node] = 0
                    elif data.gmaps:
                        orig = data.locations[from_node]
                        dest = data.locations[to_node]
                        lat0 = str(orig[0]); lon0 = str(orig[1]); lat1 = str(dest[0])
                        lon1 = str(dest[1])                
                        segm = lat0 + ',' + lon0 + '/' + lat1 + ',' + lon1
                        self._distances[from_node][to_node] = data.dmtx[0].get(segm)['distance']                     
                    else:
                        self._distances[from_node][to_node] = (distance(
                                        data.locations[from_node],
                                        data.locations[to_node]))
    
    def distance_evaluator(self, from_node, to_node):
        """Returns the manhattan distance between the two nodes"""
        return self._distances[from_node][to_node]
       
class CreateDemandEvaluator(object):
    def __init__(self, data):
        self._demands = data.demands
        
    def demand_evaluator(self, from_node, to_node):
        del to_node
        return self._demands[from_node]
    
class ConsolePrinter():
    """Print solution to console"""
    def __init__(self, data, routing, assignment):
        """Initializes the printer"""
        self._data = data
        self._routing = routing
        self._assignment = assignment

    @property
    def data(self):
        """Gets problem data"""
        return self._data

    @property
    def routing(self):
        """Gets routing model"""
        return self._routing

    @property
    def assignment(self):
        """Gets routing model"""
        return self._assignment

    def print(self):
        """Prints assignment on console"""
        # Inspect solution.
        capacity_dimension = self.routing.GetDimensionOrDie('Capacity')
        time_dimension = self.routing.GetDimensionOrDie('Time')
        total_dist = 0
        total_time = 0
        total_orders = 0
        total_vehicles = 0
        wo = self.data.wo_id
        for vehicle_id in xrange(self.data.num_vehicles):
            if self.routing.IsVehicleUsed(self.assignment, vehicle_id):
                index = self.routing.Start(vehicle_id)
                plan_output = 'Route for vehicle {0}:\n'.format(vehicle_id)
                route_dist = 0
                orders = 0
                total_vehicles += 1
                while not self.routing.IsEnd(index):
                    node_index = self.routing.IndexToNode(index)
                    next_node_index = self.routing.IndexToNode(
                            self.assignment.Value(self.routing.NextVar(index)))
                    route_dist += distance(
                            self.data.locations[node_index],
                            self.data.locations[next_node_index])
                    load_var = capacity_dimension.CumulVar(index)
                    route_load = self.assignment.Value(load_var)
                    time_var = time_dimension.CumulVar(index)
                    time_min = self.assignment.Min(time_var)
                    time_max = self.assignment.Max(time_var)
                    orders = 1 + orders
                    segment_dist = distance(
                            self.data.locations[node_index],
                            self.data.locations[next_node_index])
                    time_finish_earliest = self.data.time_per_demand_unit[node_index] + time_min
                    time_finish_latest = self.data.time_per_demand_unit[node_index] + time_max
                    transit_time = segment_dist / 500 + 5# velocidade
                    wo_id = wo[node_index]
#                    location = self.data.locations[node_index]
                    plan_output += ' Node {3} STime {0} {4}, FTime {1} {5} TTime {2} Dist {6}\n '.format(time_min, time_finish_earliest, transit_time,
                                          wo_id, time_max, time_finish_latest, segment_dist)
                    print()                 
                    write_csv(vehicle_id, wo_id, time_min, time_max, time_finish_earliest, 
                              time_finish_latest, transit_time, segment_dist)
                    index = self.assignment.Value(self.routing.NextVar(index))
                    
                print('Orders {0}'.format(orders))    
                node_index = self.routing.IndexToNode(index)
                load_var = capacity_dimension.CumulVar(index)
                route_load = self.assignment.Value(load_var)
                time_var = time_dimension.CumulVar(index)
                route_time = self.assignment.Value(time_var)
                time_min = self.assignment.Min(time_var)
                time_max = self.assignment.Max(time_var)
                total_dist += route_dist
                total_time += route_time
                total_orders += orders
                plan_output += ' {0} Load({1}) Time({2},{3})\n'.format(
                        node_index, route_load, time_min, time_max)
                plan_output += 'Distance of the route: {0}m\n'.format(route_dist)
                plan_output += 'Load of the route: {0}\n'.format(route_load)
                plan_output += 'Time of the route: {0}min\n'.format(route_time)
                print(plan_output)
            else:
                continue
        print('Total Distance of all routes: {0}m'.format(total_dist))
        print('Total Time of all routes: {0}min'.format(total_time))
        print('Total Orders Scheduled: {0}'.format(total_orders))
        print('Total Vehicles Used: {0}'. format(total_vehicles))
        
def add_distance_dimension(routing, distance_evaluator):
    """Add Global Span constraint"""
    distance = "Distance"
    maximum_distance = 6000000000000
    routing.AddDimension(
        distance_evaluator,
        0, # null slack
        maximum_distance, # maximum distance per vehicle
        True, # start cumul to zero
        distance)
    #distance_dimension = routing.GetDimensionOrDie(distance)
    # Try to minimize the max distance among vehicles.
    # /!\ It doesn't mean the standard deviation is minimized
    #distance_dimension.SetGlobalSpanCostCoefficient(10)

def add_capacity_constraints(routing, data, demand_evaluator):
    """Adds capacity constraint"""
    capacity = "Capacity"
    routing.AddDimension(
        demand_evaluator,
        0, # null capacity slack
        data.vehicle.capacity, # vehicle maximum capacity
        True, # start cumul to zero
        capacity)

def write_csv(vehicle_nbr, wo_id, tmin, tmax, time_finish_earliest, 
              time_finish_latest, transit_time, segment_dist):
    with open('output.csv', 'a', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',
                            quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow([vehicle_nbr] + [wo_id] + [tmin] + [tmax] + 
                            [time_finish_earliest] + [time_finish_latest] + 
                            [transit_time] + [segment_dist])

# Classe que exporta o output para json
class OutputWrite():
    """Print solution to console"""
    def __init__(self, data, routing, assignment, end_time, inicio_time):
        """Initializes the printer"""
        self._data = data
        self._routing = routing
        self._assignment = assignment
        self._executionFinish = end_time.strftime("%Y-%m-%d %H:%M:%S")
        self._executionStart = inicio_time.strftime("%Y-%m-%d %H:%M:%S")
        
    @property
    def data(self):
        """Gets problem data"""
        return self._data

    @property
    def routing(self):
        """Gets routing model"""
        return self._routing

    @property
    def assignment(self):
        """Gets routing model"""
        return self._assignment

    @property
    def executionFinish(self):
        """Gets routing model"""
        return self._executionFinish

    def write(self):
        time_dimension = self.routing.GetDimensionOrDie('Time')
        total_dist = 0
        total_triptime = 0
#        total_orders = 0
        total_vehicles = 0
        total_worktime = 0
        total_routetime = 0
        task_list = []
        task_assgn = []
        tgdate = self.data.targetdate
        daystart = datetime.strptime(self.data.targetdate, '%Y-%m-%d %H:%M:%S')
        daystart += timedelta(seconds = 8*60*60)
        for vehicle_id in xrange(self.data.num_vehicles):
            if self.routing.IsVehicleUsed(self.assignment, vehicle_id):
                index = self.routing.Start(vehicle_id)
                route_dist = 0
                route_triptime = 0
                route_worktime = 0
                total_vehicles += 1
                travelTime = 0
                while not self.routing.IsEnd(index):
                    pretravelTime = travelTime
                    node_dict = dict()
                    node_index = self.routing.IndexToNode(index)
                    next_node_index = self.routing.IndexToNode(
                            self.assignment.Value(self.routing.NextVar(index)))
                    if self.data.gmaps:
                        orig = self.data.locations[node_index]
                        dest = self.data.locations[next_node_index]
                        lat0 = str(orig[0]); lon0 = str(orig[1]); lat1 = str(dest[0])
                        lon1 = str(dest[1])                
                        segm = lat0 + ',' + lon0 + '/' + lat1 + ',' + lon1
                        tripDistance = self.data.dmtx[0].get(segm)['distance']
                        if self.data.traffic:
                            key = 'duration_in_traffic'
                        else:
                            key = 'duration'
                        travelTime = self.data.dmtx[0].get(segm)[key]
                        if tripDistance < 1:
                            travelTime = 5
                    else:
                        tripDistance = distance(
                            self.data.locations[node_index],
                            self.data.locations[next_node_index])
                        travelTime = int(tripDistance / 500)
                        if tripDistance < 1:
                            travelTime = 5
                    longitude = self.data.locations[node_index][1]
                    latitude = self.data.locations[node_index][0]
                    number = self.data.wo_id[node_index]
                    team = self.data.vehicle.name[vehicle_id]
                    team_code = self.data.vehicle.code[vehicle_id]
                    duration = self.data.time_per_demand_unit[node_index]
                    route_worktime += duration
                    time_var = time_dimension.CumulVar(index)
                    time_min = self.assignment.Min(time_var)
                                                            
                    if not self.routing.IsStart(index):
                        preScheduledEndTime = scheduledEndTime
                    else:
                        preScheduledEndTime = 0
                                        
                    scheduledStartTime = time_min
                    scheduledEndTime = scheduledStartTime + duration
                    tripStart = int(preScheduledEndTime)
                    tripFinish = tripStart + pretravelTime                                 
                    
                    task_assgn.append(number)
                    
                    if number == team:
                       number = "home"
                       tripStart = 0
                       next_workordertype = self.data.workordertype[next_node_index]
                       next_next_node_index = self.routing.IndexToNode(self.assignment.Value(self.routing.NextVar(next_node_index)))
                       next_next_number = self.data.wo_id[next_next_node_index]
                       if next_workordertype == 'lunch' and next_next_number == 'home':
                           break
                    if self.data.workordertype[node_index] == 'lunch':
                       number = 'lunch'
                       tripStart = int(tripFinish - travelTime)

                    node_dict['longitude'] = str(longitude)
                    node_dict['latitude'] = str(latitude)
                    node_dict['number'] = number
                    node_dict['team'] = team_code
                    node_dict['actualDuration'] = duration
                    node_dict['workorderType'] = self.data.workordertype[node_index]
                    node_dict['scheduledFinishDate'] = datetime.strftime(timedelta(seconds = scheduledEndTime*60) + daystart, '%Y-%m-%d %H:%M:%S')
                    node_dict['scheduledStartDate'] = datetime.strftime(timedelta(seconds = scheduledStartTime*60) + daystart, '%Y-%m-%d %H:%M:%S')
                    node_dict['tripDistance'] = tripDistance
                    if number != "home":
                        if number == "lunch":
                            node_dict.pop('longitude',None)
                            node_dict.pop('latitude',None)  
                        node_dict['tripstart'] = datetime.strftime(timedelta(seconds = tripStart*60) + daystart, '%Y-%m-%d %H:%M:%S')
                        node_dict['tripfinish'] = datetime.strftime(timedelta(seconds = tripFinish*60) + daystart, '%Y-%m-%d %H:%M:%S')
                        if self.data.gmaps == 1:
                            routemethod = 'GAPI'
                        else:
                            routemethod = 'L1'  
                        node_dict['routeMethod'] = routemethod
                        if number != 'lunch':
                            node_dict['priority'] = str(self.data.priority[node_index])
                            reqSkill = self.data.skill[node_index]
                            if reqSkill == '|':
                                reqSkill = ''
                            node_dict['longitude'] = str(longitude)
                            node_dict['latitude'] = str(latitude) 
                            node_dict['reqSkill'] = reqSkill.split("|")
                            node_dict['ReqSkill'] = None
                            node_dict['activityType'] = self.data.acti[node_index]
                            node_dict['name'] = self.data.task_name[node_index]
                            node_dict['networkUnit'] = self.data.networkU
                            node_dict['optimizerSolution'] = self.data.task_optSol[node_index]
                            node_dict['possibleStartDate'] = self.data.possibleStartDate[node_index]
                            node_dict['possibleFinishDate'] = self.data.possibleFinishDate[node_index]
                            node_dict['scheduledWithClient'] = self.data.client[node_index]
                            node_dict['workcenter'] = self.data.workCenter[node_index]
                            node_dict['workOrderSubtype'] = self.data.workOrderSubtype [node_index]
                            node_dict['workOrderStatus'] = self.data.workOrderStatus[node_index]                 
                            node_dict['code'] = self.data.task_code[node_index] 
                            
                    task_list.append(node_dict)
                    
                    route_dist += tripDistance
                    route_triptime += travelTime
                    
                    index = self.assignment.Value(self.routing.NextVar(index))
                
                pretravelTime = travelTime
                node_dict = dict()
                node_index = self.routing.IndexToNode(index)
                longitude = self.data.locations[node_index][1]
                latitude = self.data.locations[node_index][0]
                number = self.data.wo_id[node_index]
                team = self.data.vehicle.name[vehicle_id]
                team_code = self.data.vehicle.code[vehicle_id]
                duration = self.data.time_per_demand_unit[node_index]
                route_worktime += duration
                time_var = time_dimension.CumulVar(index)
                time_min = self.assignment.Min(time_var)
                                                        
                if not self.routing.IsStart(index):
                    preScheduledEndTime = scheduledEndTime
                else:
                    preScheduledEndTime = 0
                                
                scheduledStartTime = time_min
                scheduledEndTime = scheduledStartTime + duration
                tripStart = int(preScheduledEndTime)
                tripFinish = tripStart + pretravelTime                                 
                
                task_assgn.append(number)
                
                node_dict['longitude'] = str(longitude)
                node_dict['latitude'] = str(latitude)
                number = "home"
                node_dict['number'] = number
                node_dict['team'] = team_code
                node_dict['duration'] = duration
                node_dict['workorderType'] = self.data.workordertype[node_index]
                node_dict['scheduledFinishDate'] = datetime.strftime(timedelta(\
                         seconds = scheduledEndTime*60) \
                            + daystart, '%Y-%m-%d %H:%M:%S')
                node_dict['scheduledStartDate'] = datetime.strftime(\
                         timedelta(seconds = scheduledStartTime*60) \
                         + daystart, '%Y-%m-%d %H:%M:%S')
                node_dict['tripDistance'] = 0
                node_dict['tripstart'] = datetime.strftime(timedelta(\
                         seconds = tripStart*60) \
                + daystart, '%Y-%m-%d %H:%M:%S')
                node_dict['tripfinish'] = datetime.strftime(\
                         timedelta(seconds = tripFinish*60) \
                         + daystart, '%Y-%m-%d %H:%M:%S')
                task_list.append(node_dict)
                    
                route_dist += tripDistance
                route_triptime += travelTime
                node_dict = dict()
                
                node_dict['duration'] = 0
                node_dict['latitude'] = None
                node_dict['longitude'] = None
                node_dict['number'] = "schedule"
                node_dict['team'] = team_code
                node_dict['workOrderType'] = "schedule"
                node_dict['scheduledFinishDate'] = datetime.strftime(timedelta(\
                         seconds = (self.data.vehicle.startshift[vehicle_id]-8*60)*60) \
                            + daystart, '%Y-%m-%d %H:%M:%S')
                node_dict['scheduledStartDate'] = datetime.strftime(timedelta(\
                         seconds = (self.data.vehicle.endshift[vehicle_id]-8*60)*60) \
                            + daystart, '%Y-%m-%d %H:%M:%S')
                
                task_list.append(node_dict)
                
                total_dist += route_dist
                total_triptime += route_triptime
                total_worktime += route_worktime
                time_var = time_dimension.CumulVar(index)
                total_routetime += self.assignment.Value(time_var)
                
        total_freetime = (total_routetime - total_triptime - total_worktime)
        task_unssgn = []
        for order in self.data.wo_id:
            chck = order in task_assgn
            chck_team = order in self.data.vehicle.name
            if not chck and not chck_team :
                task_unssgn.append(order)
        
        out = dict()
        out['targetDate'] = tgdate
        out['executionFinish'] = self._executionFinish
        out['executionStart'] = self._executionStart
        out['tasks'] = task_list
        totalWorkTime_cost = total_worktime*self.data.cPMTsk
        totalDist_cost = total_dist * self.data.cPM * self.data.distcost
        totalTripTime_cost = total_triptime * self.data.cPMTrp
        totalFreeTime_cost = total_freetime*self.data.cPMFr
        out['score'] = totalWorkTime_cost + totalDist_cost + (totalTripTime_cost + totalFreeTime_cost) * self.data.timecost
        out['unscheduled'] = task_unssgn 
        out['totalDistance'] = total_dist
        out['totalWorkTime'] = total_worktime
        out['totalTripTime'] = total_triptime
        out['totalFreeTime']  = total_freetime
        out['targetWorkCenter'] = self.data.workCenter[node_index]
        
        opt_setup = dict()
        opt_setup['code'] = self.data.optCode
        opt_setup['cost'] = str(int(self.data.cost))
        if self.data.cPM == 0.01: #para que no caso de default apareca NULL
            opt_setup['costPerMeter'] = None
        else:
            opt_setup['costPerMeter'] = self.data.cPM
       
        if self.data.cPMFr == 0.03:
            opt_setup['costPerMinuteFree'] = None
        else:
            opt_setup['costPerMinuteFree'] = self.data.cPMFr
            
        if self.data.cPMTsk == 0.01:
            opt_setup['costPerMinuteTask'] = None
        else:
            opt_setup['costPerMinuteTask'] = self.data.cPMTsk
            
        if self.data.cPMTrp == 0.05:
            opt_setup['costPerMinuteTrip'] = None
        else:    
            opt_setup['costPerMinuteTrip'] = self.data.cPMTrp
            
        opt_setup['defaultSetup'] = self.data.defSetup
        opt_setup['description'] = self.data.descrip
        opt_setup['distance'] = str(self.data.distcost)
        opt_setup['externalTeams'] = str(self.data.extTeams)
        opt_setup['freeDepot'] = str(int(self.data.freeDepot))
        opt_setup['id'] = self.data.optid
        opt_setup['maps'] = str(int(self.data.gmaps))
        opt_setup['name'] = self.data.optName
        opt_setup['networkUnit'] = self.data.networkU
        opt_setup['orgunit'] = self.data.orgunit
        opt_setup['slaTolerance'] = self.data.sla
        opt_setup['start'] = self._executionStart
        opt_setup['targetDate'] = tgdate
        opt_setup['teamEfficiency'] = str(self.data.eff_toggle)
        opt_setup['time'] = str(self.data.timecost)
        opt_setup['traffic'] = str(int(self.data.traffic))
        opt_setup['useCache'] = str(int(self.data.gcache))
        opt_setup['useExtension'] = str(int(self.data.extTTable))
        opt_setup['withLunch'] = str(int(self.data.lunch))
        out['optimizerSetup'] = opt_setup
        
        return(out)
        
def opt(json_data):       
    data = DataProblem(json_data) # instanciar os dados do problema
    
    # Setup routing model
    routing = pywrapcp.RoutingModel(data.num_locations, data.num_vehicles, 
                                    data.depot, data.depot)
    # Setup solver    
    solver = pywrapcp.Solver('Solver para suportar os breaks')
    # Precomputation
    print('Computing distances')
    distance_evaluator = CreateDistanceEvaluator(data).distance_evaluator
    add_distance_dimension(routing, distance_evaluator)
    routing.SetArcCostEvaluatorOfAllVehicles(distance_evaluator)
    print('Computing times')
    time_evaluator = CreateTimeEvaluator(data).time_evaluator
    add_time_window_constraints(routing, data, time_evaluator, solver)    
    print("Constraining demand")
    demand_evaluator = CreateDemandEvaluator(data).demand_evaluator
    add_capacity_constraints(routing, data, demand_evaluator)

    # Priority constraint
    # Falta testar
    for a in range(len(data.priority)):
        node_1 = routing.IndexToNode(a)
        for b in range(len(data.priority)):
            node_2 = routing.IndexToNode(b)
            if node_1 != node_2 and data.priority[a] > data.priority[b] and \
            data.time_windows[a][0] == data.time_windows[b][0] and a not in data.lunch_loc and \
            b not in data.lunch_loc:
                solver.Add(routing.CumulVar(node_1, "Time") <= routing.CumulVar(node_2, "Time"))   
    
    # Skill set constraint    
    for a in range(0, data.num_locations):
        if a not in data.depot and a not in data.lunch_loc:
            location_idx = routing.NodeToIndex(a)
            wo_skill = data.skill[a]
            skill_match = [-1]  
            for b in range(0, data.num_vehicles):
                veh_skill = data.vehicle.skill[b]            
                if not wo_skill and not veh_skill: # skills em branco são feitas por equipas com skills em branco
                    skill_match.append(b)
                elif wo_skill in veh_skill: 
                        skill_match.append(b)
            routing.VehicleVar(location_idx).SetValues(skill_match)
       
           
    # Penalty (drop non scheduled orders) a penalidade tem de ser equilibrada
    penalty = 10000000
    for i in range(0, data.num_locations):
       if i not in data.lunch_loc or data.client[i] != 1:
           location_index = routing.NodeToIndex(i)
           routing.AddDisjunction([location_index], penalty)
           
    print("Setting search parameters")
    search_parameters = pywrapcp.RoutingModel.DefaultSearchParameters()
    search_parameters.first_solution_strategy = (
           routing_enums_pb2.FirstSolutionStrategy.GLOBAL_CHEAPEST_ARC)
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.OBJECTIVE_TABU_SEARCH)
    
#    search_parameters.local_search_operators.use_path_lns = True
#    search_parameters.local_search_operators.use_inactive_lns = False
#    search_parameters.local_search_operators.use_tsp_opt = False
    search_parameters.time_limit_ms = 1 * 60 * 1000 # x minute * 60 * 1000
#    search_parameters.use_light_propagation = False
#    search_parameters.use_depth_first_search = True # para encontrar a solução ótima, descarta o local search
 
    print("Starting solver")
    inicio_time = datetime.now()
    assignment = routing.SolveWithParameters(search_parameters)
    end_time = datetime.now()
    
#    printer = ConsolePrinter(data, routing, assignment)
#    printer.print()
    
    print(inicio_time)
    print(end_time)
    
    print("Writing to json")
    out = OutputWrite(data, routing, assignment, end_time, inicio_time)
    output = out.write()        
    return(output)
    
## Main
def main():
#    filename = 'f1W9EpbG22E=.json'    
    filename = sys.argv[1]
    with open(filename)as json_file:
            json_data = json.load(json_file)
    output = opt(json_data)
#    print(output)
#    filename_out = 'output.json'
    filename_out = sys.argv[2]
    with open(filename_out, 'w') as outfile:
            json.dump(output, outfile, sort_keys = True, indent = 4)

if __name__ == '__main__':
  main()   