﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using com.tekever.more.util.dotnet;
using com.tekever.more.connector.dotnet;
using System.Collections;
using com.tekever.more.util.dotnet.admin;
using static com.tekever.more.connector.python.oss.optimizer.Utils.Constants;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace com.tekever.more.connector.python.oss.optimizer
{
    class Optimizer : Connector
    {
        private static readonly Log log = new Log();

        private static string moreUser;
        private static string morePassword;
        private static string moreOrgunit;
        private static string storeSolutionProcess;
        private static string getDistanceMatrixProcess;
        private static bool cleanup;
        private static bool howManyTeams;
        private static util.dotnet.ThreadPool pool = new util.dotnet.ThreadPool(
            "PipeReader", 2, 100, 10 * 60 * 1000);

        public override string register()
        {
            return register("Optimizer");
        }

        public override void init()
        {
            moreUser = Properties.getStringValue("more.user");
            morePassword = Properties.getStringValue("more.password");
            moreOrgunit = Properties.getStringValue("more.orgunit");
            storeSolutionProcess = Properties.getStringValue("optimizer.storesolutionprocess");
            getDistanceMatrixProcess = Properties.getStringValue("optimizer.getdistancematrix");

            cleanup = Properties.getBoolValue("optimizer.cleanup", true);

            base.init();
            addOperation("optimize", optimize);
        }

        public ServiceMessage optimize(IDictionary parameters)
        {
            IList destinations = new ArrayList();
            howManyTeams = NumericUtil.parseBool(parameters[Attr.howManyTeams]);
            moreOrgunit = string.IsNullOrEmpty((string)((IDictionary)parameters[Attr.otherParams])[Attr.orgunit]) ? moreOrgunit : (string)((IDictionary)parameters[Attr.otherParams])[Attr.orgunit];
            foreach (string key in ((IDictionary)parameters[Attr.otherParams]).Keys)
                Log.Debug(log, "> " + key + ": " + ((IDictionary)parameters[Attr.otherParams])[key]);

            #region Get optimizer constraints
            ((IDictionary)parameters[Attr.otherParams])[Attr.abBuffer] = NumericUtil.parseInt(((IDictionary)parameters[Attr.otherParams])[Attr.abBuffer], 15);
            ((IDictionary)parameters[Attr.otherParams])[Attr.slaTolerance] = NumericUtil.parseInt(((IDictionary)parameters[Attr.otherParams])[Attr.slaTolerance], 0);
            ((IDictionary)parameters[Attr.otherParams])[Attr.lunchTime] = NumericUtil.parseInt(((IDictionary)parameters[Attr.otherParams])[Attr.lunchTime], 60);
            ((IDictionary)parameters[Attr.otherParams])[Attr.targetDate] = NumericUtil.parseDateTime(((IDictionary)parameters[Attr.otherParams])[Attr.targetDate]).ToString("yyyy-MM-dd HH:mm:ss");

            ((IDictionary)parameters[Attr.otherParams])[Attr.costPerMeter] = NumericUtil.parseDouble(((IDictionary)parameters[Attr.otherParams])[Attr.costPerMeter], Properties.getDoubleValue("optimizer.costs.costPerMeter", 0.06392));
            ((IDictionary)parameters[Attr.otherParams])[Attr.costPerMinuteTask] = NumericUtil.parseDouble(((IDictionary)parameters[Attr.otherParams])[Attr.costPerMinuteTask], Properties.getDoubleValue("optimizer.costs.costPerMinuteTask", 0.104));
            ((IDictionary)parameters[Attr.otherParams])[Attr.costPerMinuteFree] = NumericUtil.parseDouble(((IDictionary)parameters[Attr.otherParams])[Attr.costPerMinuteFree], Properties.getDoubleValue("optimizer.costs.costPerMinuteFree", 0.208));
            ((IDictionary)parameters[Attr.otherParams])[Attr.costPerMinuteTrip] = NumericUtil.parseDouble(((IDictionary)parameters[Attr.otherParams])[Attr.costPerMinuteTrip], Properties.getDoubleValue("optimizer.costs.costPerMinuteTrip", 0.104));
            parameters[Attr.targetDate] = NumericUtil.parseDateTime(parameters[Attr.targetDate]).ToString("yyyy-MM-dd HH:mm:ss");
            #endregion

            #region Handle Destinations
            IList tasks = (IList)parameters[Attr.tasks];
            if(tasks.Count > 0)
            {
                try
                {
                    string getCoordinatesProcess = Properties.getStringValue("optimizer.getcoordinatesprocess");
                    Log.Info(log, "getCoordinatesProcess: " + getCoordinatesProcess);
                    IDictionary getCoordinatesProcessParams = new Hashtable();
                    getCoordinatesProcessParams.Add("operation", "getAddressByLocation");
                    getCoordinatesProcessParams.Add("orderLimit", "-1");
                    getCoordinatesProcessParams.Add("hasClosed", "false");
                    getCoordinatesProcessParams.Add("tasks", tasks);
                    getCoordinatesProcessParams.Add("targetWorkCenter", (string)parameters[Attr.targetWorkCenter]);
                    ServiceMessage response = executeProcess(moreUser, morePassword, moreOrgunit, null, null, getCoordinatesProcess, getCoordinatesProcessParams);
                    Log.Info(log, response.getString());
                    Log.Debug(log, "tasks.Count: " + tasks.Count);
                    
                    if (!string.IsNullOrEmpty(((IDictionary)response.getUser()[0])[Attr.tasks].ToString()))
                        tasks = (IList)((IDictionary)response.getUser()[0])[Attr.tasks];

                    Log.Debug(log, "tasks.Count: " + tasks.Count);
                }
                catch (Exception ex)
                {
                    Log.Error(log, "Get coordinates: " + ex);
                }
            }
            parameters[Attr.tasks] = tasks;

            IDictionary SkillsToList = new Hashtable();
            IList teams = (IList)parameters[Attr.teams];
            IList tasksToRemove = new ArrayList();
            IList teamsToRemove = new ArrayList();

            DateTime targetDate = NumericUtil.parseDateTime(parameters[Attr.targetDate]);
            if (teams != null && !howManyTeams)
            {
                foreach (IDictionary team in teams)
                {
                    //team skills is a string, transform it to a list
                    if (string.IsNullOrEmpty((string)team[Attr.skills]))
                    {
                        team[Attr.skills] = "";
                    }

                    //Log.Dump(log, "otherParams[Attr.teamEfficiency] " + otherParams[Attr.teamEfficiency]);
                    if ((string)((IDictionary)parameters[Attr.otherParams])[Attr.teamEfficiency] == "0" || string.IsNullOrEmpty((string)team[Attr.coef]))
                    {
                        team[Attr.coef] = "1";
                    }
                    SeparateSkills(SkillsToList, team, Attr.skills);

                    if (NumericUtil.parseDateTime(team[Attr.possibleStartDate]) > NumericUtil.parseDateTime(team[Attr.possibleFinishDate])
                        || team[Attr.latitude] == null
                        || team[Attr.longitude] == null
                        || NumericUtil.parseDouble(team[Attr.latitude]) == 0
                        || NumericUtil.parseDouble(team[Attr.longitude]) == 0)
                    {
                        teamsToRemove.Add(team);
                    }

                    if (team[Attr.latitude] != null
                        && team[Attr.longitude] != null
                        && NumericUtil.parseDouble(team[Attr.latitude]) != 0
                        && NumericUtil.parseDouble(team[Attr.longitude]) != 0
                        )
                    {
                        team[Attr.latitude] = NumericUtil.parseDouble(team[Attr.latitude]).ToString().Replace(',', '.');
                        team[Attr.longitude] = NumericUtil.parseDouble(team[Attr.longitude]).ToString().Replace(',', '.');

                        IDictionary point = new Hashtable();
                        point[Attr.point] = team[Attr.latitude] + "," + team[Attr.longitude];
                        point[Attr.timeBlock] = 1;
                        destinations.Add(point);
                    }

                    if (team[Attr.intra_lat] != null
                        && team[Attr.intra_lon] != null
                        && NumericUtil.parseDouble(team[Attr.intra_lat]) != 0
                        && NumericUtil.parseDouble(team[Attr.intra_lon]) != 0
                        )
                    {
                        team[Attr.intra_lat] = NumericUtil.parseDouble(team[Attr.intra_lat]).ToString().Replace(',', '.');
                        team[Attr.intra_lon] = NumericUtil.parseDouble(team[Attr.intra_lon]).ToString().Replace(',', '.');

                        IDictionary point = new Hashtable();
                        point[Attr.point] = team[Attr.intra_lat] + "," + team[Attr.intra_lon];
                        point[Attr.timeBlock] = 1;
                        destinations.Add(point);
                    }
                }
                if (SkillsToList.Keys.Count == 0)
                    Log.Error(log, "No skills where found in teams!");
            }
            else
            {
                if (parameters[Attr.home_lat] != null
                        && parameters[Attr.home_lon] != null
                        && NumericUtil.parseDouble(parameters[Attr.home_lat]) != 0
                        && NumericUtil.parseDouble(parameters[Attr.home_lon]) != 0
                        )
                {
                    parameters[Attr.home_lat] = NumericUtil.parseDouble(parameters[Attr.home_lat]).ToString().Replace(',', '.');
                    parameters[Attr.home_lon] = NumericUtil.parseDouble(parameters[Attr.home_lon]).ToString().Replace(',', '.');
                }
                    destinations.Add(parameters[Attr.home_lat] + "," + parameters[Attr.home_lon]);
            }
            if (tasks != null)
            {
                
                foreach (IDictionary task in tasks)
                {
                    if (string.IsNullOrEmpty((string)task[Attr.reqSkill]))
                        task[Attr.reqSkill] = "";
                    //reqSkills is a string, and it transforms it to a list
                    //Log.Dump(log, "Task " + task[Attr.code] + " " + task[Attr.name] + " - skills:" + task[Attr.reqSkill]);
                    SeparateSkills(SkillsToList, task, Attr.reqSkill);
                    if (NumericUtil.parseDateTime(task[Attr.possibleStartDate]) > NumericUtil.parseDateTime(task[Attr.possibleFinishDate])
                        || task[Attr.latitude] == null
                        || task[Attr.longitude] == null
                        || NumericUtil.parseDouble(task[Attr.latitude]) == 0
                        || NumericUtil.parseDouble(task[Attr.longitude]) == 0)
                    {
                        tasksToRemove.Add(task);
                    }
                    if (task[Attr.latitude] != null
                        && task[Attr.longitude] != null
                        && NumericUtil.parseDouble(task[Attr.latitude]) != 0
                        && NumericUtil.parseDouble(task[Attr.longitude]) != 0
                        )
                    {
                        task[Attr.latitude] = NumericUtil.parseDouble(task[Attr.latitude]).ToString().Replace(',', '.');
                        task[Attr.longitude] = NumericUtil.parseDouble(task[Attr.longitude]).ToString().Replace(',', '.');

                        //Log.Debug(log, "destination task: " + task[Attr.latitude] + "," + task[Attr.longitude]);
                        IDictionary point = new Hashtable();
                        point[Attr.point] = task[Attr.latitude] + "," + task[Attr.longitude];

                        if(NumericUtil.parseDateTime(task[Attr.possibleStartDate]).Hour <= 10 || !NumericUtil.parseBool(task[Attr.scheduledWithClient], true))
                        {
                            point[Attr.timeBlock] = 1;
                        }
                        else if(NumericUtil.parseDateTime(task[Attr.possibleStartDate]).Hour <= 13)
                        {
                            point[Attr.timeBlock] = 2;
                        }
                        else if (NumericUtil.parseDateTime(task[Attr.possibleStartDate]).Hour <= 15)
                        {
                            point[Attr.timeBlock] = 3;
                        }
                        else if (NumericUtil.parseDateTime(task[Attr.possibleStartDate]).Hour <= 18)
                        {
                            point[Attr.timeBlock] = 4;
                        }
                        else
                        {
                            point[Attr.timeBlock] = 1;
                        }

                        destinations.Add(point);
                    }
                }
                if (SkillsToList.Keys.Count == 0)
                {
                    Log.Error(log, "No skills where found in tasks!");
                }

                foreach (IDictionary team in teamsToRemove)
                {
                    teams.RemoveAt(teams.IndexOf(team));
                    //Log.Debug(log, "tasks.IndexOf(task): " + tasks.IndexOf(task));
                }

                //foreach (IDictionary task in tasksToRemove)
                //{
                //    tasks.RemoveAt(tasks.IndexOf(task));
                //    //Log.Debug(log, "tasks.IndexOf(task): " + tasks.IndexOf(task));
                //}
                //Log.Debug(log, "tasks count: " + tasks.Count);
            }
            #endregion

            #region Calculate Distance Matrix
            IDictionary logParams = new Hashtable();
            logParams.Add("logMaster", (string)((IDictionary)parameters[Attr.otherParams])[Attr.logMaster]);
            logParams.Add("cronSetupCode", (string)((IDictionary)parameters[Attr.otherParams])[Attr.cronSetupCode]);
            logParams.Add("targetDate", ((IDictionary)parameters[Attr.otherParams])[Attr.targetDate]);
            logParams.Add("workCenter", (string)parameters[Attr.targetWorkCenter]);
            logParams.Add("user", (string)((IDictionary)parameters[Attr.otherParams])[Attr.user]);
            if (NumericUtil.parseBool(((IDictionary)parameters[Attr.otherParams])[Attr.maps], false))
            {
                if (!parameters.Contains("matrix"))
                {
                    IDictionary otherParams = (IDictionary) parameters[Attr.otherParams];
                    IDictionary distanceMatrix = null;
                    bool useTrafic = NumericUtil.parseBool(otherParams[Attr.traffic]);
                    bool useCache = NumericUtil.parseBool(otherParams[Attr.useCache]);
                    //if it receives google maps data
                    if (NumericUtil.parseBool(otherParams[Attr.maps], false))
                    {
                        //gets response from server to see if it gets matrix
                        distanceMatrix = getDistanceMatrix(destinations, logParams, useTrafic, useCache);

                        if (distanceMatrix != null)
                        {
                            Log.Info(log, "Obtained distance matrix");
                            //Log.DumpWarn(log, distanceMatrix);
                            parameters[Attr.matrix] = distanceMatrix;
                        }
                        else
                        {
                            Log.Warn(log, "No distance matrix returned");
                        }
                    }
                }
                if(((IDictionary)(parameters[Attr.matrix])) != null)
                {
                    Log.Debug(log, "Matrix contains {0} elements", ((IDictionary)(parameters[Attr.matrix])).Keys.Count);

                    // confirm all distances are passed as int
                    foreach (IDictionary distance in ((IDictionary)(parameters[Attr.matrix])).Values)
                    {
                        object[] keys = new object[distance.Keys.Count];
                        distance.Keys.CopyTo(keys, 0);
                        foreach (object key in keys)
                        {
                            long value;
                            if (Int64.TryParse(distance[key]?.ToString(), out value))
                                distance[key] = value;
                        }
                    }
                }
            }
            #endregion

            #region Invoke Python Optimizer
            // Generate random filenames and create input file
            byte[] randomBytes = new byte[9];
            Random random = new Random();
            string inputfilename;
            string inputdir;
            do
            {
                random.NextBytes(randomBytes);
                inputfilename = Convert.ToBase64String(randomBytes).Replace('\\','-').Replace('/', '-') + ".json";
                inputdir = "optimizer-input";
            } while (File.Exists(inputdir + Path.DirectorySeparatorChar + inputfilename));

            using (StreamWriter inputFileStream = File.CreateText(inputdir + Path.DirectorySeparatorChar + inputfilename))
            {
                inputFileStream.Write(JsonConvert.SerializeObject(parameters));
            }
            //inputfilename = "teste_input21_gmap.txt";

            //random.NextBytes(randomBytes);
            string outputfilename;
            string outputdir;
            do
            {
                random.NextBytes(randomBytes);
                outputfilename = Convert.ToBase64String(randomBytes).Replace('\\', '-').Replace('/', '-') + ".json";
                outputdir = "optimizer-output";
            } while (File.Exists(outputdir + Path.DirectorySeparatorChar + outputfilename));

            //Creates the OutPut file
            using (StreamWriter outputFileStream = File.CreateText(outputdir + Path.DirectorySeparatorChar + outputfilename))
            {
                //outputFileStream.Write(JsonConvert.SerializeObject(parameters));
            }

            // Invoke python
            runPython(
                inputdir + Path.DirectorySeparatorChar + inputfilename + " " + 
                outputdir + Path.DirectorySeparatorChar + outputfilename);

            // Get response and prepare to send to server
            ServiceMessage output = new ServiceMessage(ServiceMessage.STATUS.FAILED, "No response from optimizer core.");
            if (File.Exists(outputdir + Path.DirectorySeparatorChar + outputfilename))
            {
                try {
                    // Open the file to read from.
                    string[] readText = File.ReadAllLines(outputdir + Path.DirectorySeparatorChar + outputfilename);
                    IDictionary solution = (IDictionary) parseJSON(string.Concat(readText));

                    if (solution.Contains(Attr.error))
                        output = new ServiceMessage(ServiceMessage.STATUS.ERROR, new object[] { solution[Attr.error] });
                    else
                    {
                        solution[Attr.outputfilename] = outputfilename;
                        solution[Attr.howManyTeams] = howManyTeams;
                        solution[Attr.ReOptimize] = (string)parameters[Attr.ReOptimize];
                        solution[Attr.teamCodesNotIn] = (IList)parameters[Attr.teamCodesNotIn];
                        solution[Attr.logMaster] = (string)((IDictionary)parameters[Attr.otherParams])[Attr.logMaster];
                        solution[Attr.orderLogs] = Convert.ToInt32(((IDictionary)parameters[Attr.otherParams])[Attr.orderLogs]);
                        solution[Attr.cronSetupCode] = (string)((IDictionary)parameters[Attr.otherParams])[Attr.cronSetupCode];
                        foreach (IDictionary task in tasksToRemove)
                        {
                            //tasks.RemoveAt(tasks.IndexOf(task));
                            ((IList)solution[Attr.unscheduled]).Add(task);
                        }
                        Log.Debug(log, "> " + ((IList)parameters[Attr.tasks]).Count);
                        foreach(IDictionary taskRaw in (IList)parameters[Attr.tasks])
                        {
                            foreach (IDictionary task in (IList)solution[Attr.tasks])
                            {
                                if (taskRaw[Attr.code] == task[Attr.code])
                                {
                                    task[Attr.address] = taskRaw[Attr.address];
                                }
                            }
                        }
                        //Call StoreSolution process
                        ServiceMessage response = executeProcess(moreUser, morePassword, moreOrgunit, null, null, storeSolutionProcess, solution);
                        //Returns a message to the MyFork
                        solution[Attr.orderLogs] = ((IDictionary)response.getUser()[0])[Attr.orderLogs];
                        Log.Info(log, "solution code: " + ((IDictionary)response.getUser()[0])[Attr.solution]);
                        output = new ServiceMessage(new object[] {
                            CollectionsUtil.Create(
                                "solution", ((IDictionary)response.getUser()[0])[Attr.solution], 
                                "orderLogs", ((IDictionary)response.getUser()[0])[Attr.orderLogs]),
                            response.ToString() });
                        
                        //Old line of output
                        //output = new ServiceMessage(ServiceMessage.STATUS.OK, new object[] { response });
                    }
                } catch (Exception e)
                {
                    output = new ServiceMessage(ServiceMessage.STATUS.ERROR, e.Message);
                    Log.Error(log, e.Message);
                    Log.Error(log, e.StackTrace);

                }
                if (cleanup) File.Delete(outputdir + Path.DirectorySeparatorChar + outputfilename);
            } 
                
            if (cleanup) File.Delete(inputdir + Path.DirectorySeparatorChar + inputfilename);
            #endregion

            return output;
        }

        private IDictionary getDistanceMatrix(IList destinations, IDictionary logParams, bool useTraffic = false, bool useCache = true)
        {
            //*Note!!!! origins allways equal destinations, because we need a square matrix!!! (TiagoFernandes)
            // so we don't need pass duplicated IList

            //Log.Debug(log, "moreOrgunit: " + moreOrgunit);
            //sends data to server
            ServiceMessage response = executeProcess(moreUser, morePassword, moreOrgunit, null, null, getDistanceMatrixProcess,
                CollectionsUtil.Create(new object[] {
                  //  "origins", origins,
                    "destinations", destinations,
                    "useTraffic",  useTraffic,
                    "useCache", useCache,
                    "logParams", logParams
                }));

            //gets response
            if (response != null && response.getSuccess())
            {
                return (IDictionary)response.getUser()?[0];
            } 
            return null;
        }

        private static void runPython(string args)
        {
            Thread runner = new Thread(delegate ()
            {
                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = Properties.getStringValue("python.path", "Python/python.exe"); //cmd is full path to python.exe
                Log.Debug(log, "howManyTeams: " + howManyTeams);
                if (howManyTeams)
                {
                    Log.Debug(log, "Using opt_veh.py.");
                    start.Arguments = Properties.getStringValue("python.script_veh", "opt_veh.py") + " " + args;
                }
                else
                    start.Arguments = Properties.getStringValue("python.script", "Python/opt.py") + " " + args;

                start.UseShellExecute = false;
                start.CreateNoWindow = true;
                start.RedirectStandardOutput = !start.UseShellExecute;
                start.RedirectStandardError = !start.UseShellExecute;

                Log.Debug(log, "Invoking python with args: " + start.Arguments);
                using (System.Diagnostics.Process process = System.Diagnostics.Process.Start(start))
                {
                    Log.Info(log, "Starting pool execute...");
                    pool.execute(delegate (object argument)
                    {
                        if (start.RedirectStandardOutput)
                            using (StreamReader reader = process.StandardOutput)
                            {
                                string line = null;
                                while ((line = reader.ReadLine()) != null)
                                {
                                    Log.Debug(log, line);
                                }
                            }
                    }, null);

                    Log.Info(log, "Starting pool execute...");
                    pool.execute(delegate (object argument)
                    {
                        if (start.RedirectStandardError)
                            using (StreamReader reader = process.StandardError)
                            {
                                string line = null;
                                while ((line = reader.ReadLine()) != null)
                                {
                                    Log.Debug(log, line);
                                }
                            }
                    }, null);

                    process.WaitForExit();
                    Log.Debug(log, "Optimizer core finished with exit code 0x{0:X8}", process.ExitCode);
                }
            }, 50 * 1024 * 1024);
            runner.Start();
            runner.Join();
        }

        public static object parseJSON(string json)
        {
            //JsonValue jsonObject = JsonValue.Parse(json);
            IDictionary<string, JToken> Jsondata = JObject.Parse(json);

            return convertJSONObject(Jsondata);
        }

        private static object convertJSONObject(object jsonObject)
        {
            if (jsonObject is IDictionary<string, JToken> jsonDictionary)
            {
                IDictionary dictionary = new Hashtable();
                IList dictAsList = new ArrayList();
                foreach (string key in jsonDictionary.Keys)
                {
                    dictionary[key] = convertJSONObject(jsonDictionary[key]);
                }
                return dictionary;
            }
            else
            {
                if (jsonObject is JArray array)
                {
                    IList list = new ArrayList();
                    foreach (JToken obj in array)
                    {
                        list.Add(convertJSONObject(obj));
                    }
                    return list;
                }
                else
                {
                    return jsonObject.ToString();
                }
            }
        }

        private void SeparateSkills(IDictionary skillsList, IDictionary element, string field)
        {
            IList elementSkills = new ArrayList();
            string[] skillsArray = ((string)element[field]).Split('|');
            //Log.Info(log, "element " + element[Attr.code] + " " + element[Attr.name] + " skills: " + element[field]);
            foreach (string skills in skillsArray)
            {
                elementSkills.Add(skills);
                //Log.Debug(log, "Skill: " + skills);
            }

            //Log.Info(log, "End Skill array");
            skillsList[element[Attr.code]] = elementSkills;
            element[field] = elementSkills;
        }

        public static void Main(string[] args)
        {
            if (args.Length >= 0 && args[0] != "-test")
            {
                startUp(typeof(Optimizer), args);
                return;
            }
            Properties.init();
            Log.init("Optimizer");

            Optimizer optimizer = new Optimizer();
            optimizer.init();

            string testData = File.ReadAllText("optimizer-input\\teste_input21_gmap.txt");
            IDictionary parameters = (IDictionary)parseJSON(testData);

            ServiceMessage result = optimizer.optimize(parameters);
            Log.Dump(log, result);
            Log.shutdown();
        }

    }
}
